package com.mutsuddi_s.composemovieapp.di

import com.mutsuddi_s.composemovieapp.data.MovieDataSource
import com.mutsuddi_s.composemovieapp.data.MovieRepository
import com.mutsuddi_s.composemovieapp.network.ApiService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create

@InstallIn(SingletonComponent::class)
@Module
object AppModule {

    @Provides
    fun provideRetrofit(): Retrofit{
        return Retrofit.Builder().baseUrl("https://api.themoviedb.org/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    @Provides
    fun provideApiService(retrofit: Retrofit): ApiService{
        return retrofit.create(ApiService::class.java)
    }

    @Provides
    fun providedatasource(apiService: ApiService): MovieDataSource{
        return MovieDataSource(apiService)
    }

    @Provides
    fun provideMovieRepository(dataSource: MovieDataSource): MovieRepository{
        return MovieRepository(dataSource)
    }
}