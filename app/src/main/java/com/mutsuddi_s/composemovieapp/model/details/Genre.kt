package com.mutsuddi_s.composemovieapp.model.details

data class Genre(
    val id: Int,
    val name: String
)