package com.mutsuddi_s.composemovieapp.model.details

data class ProductionCountry(
    val iso_3166_1: String,
    val name: String
)