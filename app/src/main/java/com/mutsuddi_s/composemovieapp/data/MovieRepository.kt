package com.mutsuddi_s.composemovieapp.data

import com.mutsuddi_s.composemovieapp.common.Resource
import com.mutsuddi_s.composemovieapp.model.Movie
import com.mutsuddi_s.composemovieapp.model.details.MovieDetails

class MovieRepository(private val movieDataSource: MovieDataSource) {

    suspend fun getMovieList(): Resource<List<Movie>> {

        return try {
            Resource.Success(movieDataSource.getMovieList().results)

        }catch (e:Exception) {
            Resource.Error(message = e.message.toString())
        }

    }


    suspend fun getMovieDetails(id: String): Resource<MovieDetails>{
        return try {

            Resource.Success(movieDataSource.getMovieDetails(id))

        } catch (e: Exception){
            Resource.Error(message = e.message.toString())
        }
    }
}