package com.mutsuddi_s.composemovieapp.data

import com.mutsuddi_s.composemovieapp.network.ApiService


class MovieDataSource(private val apiService: ApiService) {

    suspend fun getMovieList() = apiService.getMovieList("ed357d659652a60b4295b128bc76ba5c")

    suspend fun getMovieDetails(id: String) = apiService.getMovieDetails(id,"ed357d659652a60b4295b128bc76ba5c")
}