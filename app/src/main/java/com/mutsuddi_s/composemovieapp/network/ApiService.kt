package com.mutsuddi_s.composemovieapp.network

import com.mutsuddi_s.composemovieapp.model.MovieListResponse
import com.mutsuddi_s.composemovieapp.model.details.MovieDetails
import com.mutsuddi_s.composemovieapp.navigation.MovieNavigationItem
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {

    //https://api.themoviedb.org/3/movie/popular?api_key=ed357d659652a60b4295b128bc76ba5c

    //https://api.themoviedb.org/3/movie/{movie_id}?api_key=your_api_key

    @GET("3/movie/popular")
    suspend fun getMovieList(
        @Query("api_key") apiKey:String
    ):MovieListResponse


    @GET("/3/movie/{id}")
    suspend fun getMovieDetails(
        @Path("id") id: String,
        @Query("api_key") apiKey: String
    ): MovieDetails
}