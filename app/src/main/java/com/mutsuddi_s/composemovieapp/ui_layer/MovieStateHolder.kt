package com.mutsuddi_s.composemovieapp.ui_layer

import com.mutsuddi_s.composemovieapp.model.Movie

data class MovieStateHolder (
    val isLoading: Boolean = false,
    val data: List<Movie>? = null,
    val error: String =""
)