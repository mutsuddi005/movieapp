package com.mutsuddi_s.composemovieapp.ui_layer.details

import com.mutsuddi_s.composemovieapp.model.details.MovieDetails

data class MovieDetailsStateHolder (
    val isLoading: Boolean= false,
    val data: MovieDetails?= null,
    val error: String=""
)